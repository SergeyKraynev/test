ArrayList<Integer> aListNumbers = new ArrayList<Integer>();
aListNumbers.add(1);
aListNumbers.add(2);
aListNumbers.add(3);
 
/*
 * To copy an ArrayList to another ArrayList, use the
 * constructor having the Collection parameter
 */
ArrayList<Integer> aListCopy 
                = new ArrayList<Integer>( aListNumbers );
 
System.out.println("Copy ArrayList to another ArrayList: ");
System.out.println("Original ArrayList: " + aListNumbers);
System.out.println("Copied ArrayList: " + aListCopy);
Output
